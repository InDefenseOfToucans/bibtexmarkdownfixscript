# bibtexmarkdownfixscript

Short simple script I wrote for automating fixing the citations in markdown files I created using pandoc.


So when I run pandoc -s -o input.md input.tex

This converts the citations to look like
[@alexander1991international 785]

but they need to look like This for use with [Jekyll-Scholar](https://github.com/inukshuk/jekyll-scholar)
[{% cite deutscher2003prophetarmed %}]

When running it just grabs input.md in that folder and searches for the citations and fixes the formating to match for Jekyll-Scholar.
