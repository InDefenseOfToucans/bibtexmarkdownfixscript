import re

with open('input.md', 'r') as file:
    data = file.read()

fixedfile = data
print(re.findall(r'[@]\S*', data))

for bibtexkey in re.findall(r'[@]\S*', data):
    currentbibtexkeywithoutatsign = bibtexkey.replace('@', '')
    fixedstring = '{% cite ' + currentbibtexkeywithoutatsign + ' %}'
    print(bibtexkey)
    print('The Current fixed Key is', fixedstring)
    fixedfile = data.replace(bibtexkey, fixedstring)
    data = fixedfile

f = open("fixed.markdown", "w")
f.write(data)
f.close()
